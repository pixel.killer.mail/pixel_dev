class Nav {
  constructor(NewBus){
    this.bus = NewBus
    this.bus.subscribe('navButtonClick', this.navButtonClicked.bind(this))
  }
  navButtonClicked(detail){
    console.log(detail)
  }
}

export default Nav